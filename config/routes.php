<?php
return [
    'admin/login' => 'admin/login',
    'admin/logout' => 'admin/logout',
    'admin' => 'admin/index',
    'tasks/[?]sort=([a-z]+)&order=([a-zA-Z]+)&status=([0,1])/page-([0-9]+)' => 'tasks/index/$1/$2/$3/$4',
    'tasks/[?]sort=([a-z]+)&order=([a-zA-Z]+)/page-([0-9]+)' => 'tasks/index/$1/$2/$3',
    'tasks/[?]sort=([a-z]+)&order=([a-zA-Z]+)' => 'tasks/index/$1/$2',
    'tasks/[?]status=([0,1])/page-([0-9]+)' => 'tasks/index/$1/$2',
    'tasks/[?]status=([0,1])' => 'tasks/index/$1',
    'tasks/update/([0-9]+)' => 'tasks/update/$1',
    'tasks/delete/([0-9]+)' => 'tasks/delete/$1',
    'tasks/create' => 'tasks/create',
    'tasks/page-([0-9]+)' => 'tasks/index/$1',
    'page-([0-9])' => 'tasks/index/$1',
    'tasks' => 'tasks/index', //actionIndex in TasksController
    '' => 'tasks/index',
    //Админпанель

];