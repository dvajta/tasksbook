<?php

//Front controller
use components\Db;
use components\Router;
// 1.Общие настройки
ini_set('display_errors', 1);
error_reporting(E_ALL);

// 2.Подключение файлов системы
define('ROOT', dirname(__FILE__));
require_once __DIR__. '/vendor/autoload.php';

// 4.Вызов Router
$router = new Router();
$router->run();