<?php
namespace controllers;

use components\Pagination;
use models\Tasks;


class TasksController
{
    public function actionIndex($sort = false, $order = false, $status = false, $page = 1)
    {
        if (!$_GET) {
            if($sort && is_integer((int)$sort)){
                $page = $sort;
                $tasksList = Tasks::getTasksList($page);
            }
            $tasksList = Tasks::getTasksList($page);
        }elseif(isset($_GET['sort']) && isset($_GET['order'])){
            $order = preg_replace('~/page-[0-9]+~', '', $_GET['order']);
            $page = $status ? $status : $page;
            $sort = $_GET['sort'];
            $tasksList = Tasks::getTasksBySort($sort, $order, $page);
        }elseif(isset($_GET['status'])){
            $status = (int)$_GET['status'];
            $page = $order ? $order : $page;
            $tasksList = Tasks::getTasksByStatus($status, $page);
        }

        $total = Tasks::getTotalTasks();
        $pagination = new Pagination($total, $page, Tasks::SHOW_BY_DEFAULT, 'page-');

        require_once(ROOT. '/views/tasks/index.php');

        return true;
    }

    public function actionUpdate($id)
    {
        if($id){
            $task = Tasks::getTasksItemById($id);
            if($_SERVER['REQUEST_METHOD'] == "POST"){
                //print_r($_POST);
                if(isset($_FILES['image']) && $_FILES['image']['type'] == 'image/jpeg' || $_FILES['image']['type'] == 'image/gif' || $_FILES['image']['type'] == 'image/png'){
                    $file = 'uploads/'.$_FILES['image']['name'];
                    $extension = $_FILES['image']['type'];
                    move_uploaded_file($_FILES['image']['tmp_name'], $file);

                    $this->resize_image($file, $extension, '320');
                }
                $data = [
                    'id' => $id,
                    'status' => isset($_POST['status']) && $_POST['status'] == 'on' ? 1 : 0,
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'text' => $_POST['text'],
                    'image' => $_FILES['image']['name'] != null && $_FILES['image']['type'] == 'image/jpeg' || $_FILES['image']['type'] == 'image/gif' || $_FILES['image']['type'] == 'image/png' ? $_FILES['image']['name'] : $task['image']
                ];
                if(Tasks::updateTask($data)){
                    header("Location: /");exit;
                }

            }

            require_once(ROOT. '/views/tasks/update.php');
        }
        return true;
    }

    public function actionCreate()
    {
        if($_SERVER['REQUEST_METHOD'] == "POST"){
            if(isset($_FILES['image']) && $_FILES['image']['type'] == 'image/jpeg' || $_FILES['image']['type'] == 'image/gif' || $_FILES['image']['type'] == 'image/png'){
                $file = 'uploads/'.$_FILES['image']['name'];
                $extension = $_FILES['image']['type'];
                move_uploaded_file($_FILES['image']['tmp_name'], $file);

                $this->resize_image($file, $extension, '320');
            }
            $data = [
                'name' => $_POST['name'],
                'email' => $_POST['email'],
                'text' => $_POST['text'],
                'image' => $_FILES['image']['name'] != null && $_FILES['image']['type'] == 'image/jpeg' || $_FILES['image']['type'] == 'image/gif' || $_FILES['image']['type'] == 'image/png' ? $_FILES['image']['name'] : false
            ];

            if(Tasks::createTask($data)){
                header("Location: /");exit;
            }


        }

        return require_once(ROOT. '/views/tasks/create.php');

    }

    public function actionDelete($id)
    {
        if($id && Tasks::deleteTask($id)){
            header("Location: /");exit;
        }
        return true;
    }

    public function actionSorting($sort, $order, $status){
        if($sort && $order && $status){
            $tasksItem = Tasks::getTasksBySort($sort, $order, $status);
            require_once(ROOT. '/views/tasks/view.php');
        }
        return true;
    }

    public function resize_image($file, $extension, $max_resolution)
    {
        if(file_exists($file)){
            if($extension == 'image/jpeg'){
                $original_image = imagecreatefromjpeg($file);
            }elseif($extension == 'image/png'){
                $original_image = imagecreatefrompng($file);
            }elseif($extension == 'image/gif'){
                $original_image = imagecreatefromgif($file);
            }else false;


            //resolution
            $original_width = imagesx($original_image);
            $original_height = imagesy($original_image);

            //try width first
            $ratio = $max_resolution / $original_width;
            $new_width = $max_resolution;
            $new_height = $original_height * $ratio;

            //if that didn`t work
            if($new_height > $max_resolution){
                $ratio = $max_resolution / $original_height;
                $new_height = $max_resolution;
                $new_width = $original_width * $ratio;
            }

            if($original_image){

                $new_image = imagecreatetruecolor($new_width, $new_height);
                imagecopyresampled($new_image, $original_image,0,0,0,0, $new_width, $new_height, $original_width, $original_height);

                if($extension == 'image/jpeg'){
                    imagejpeg($new_image, $file, 90);
                }elseif($extension == 'image/png'){
                    imagepng($new_image, $file);
                }elseif($extension == 'image/gif'){
                    imagegif($new_image, $file);
                }else false;
            }

        }
    }
}