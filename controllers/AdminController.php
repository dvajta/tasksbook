<?php


namespace controllers;


use models\User;

class AdminController
{
    public function actionIndex()
    {
        if(!isset($_SESSION['auth'])){
            //print_r($_SESSION['auth']);
            //Подключение вида
            header('HTTP/1.1 200 OK');
            header('Location: http://'.$_SERVER['HTTP_HOST'].'/admin/login');

        }else{
            header('Location: http://'.$_SERVER['HTTP_HOST'].'/');
        }

        return true;
    }

    public function actionLogin()
    {
        if($_SERVER['REQUEST_METHOD'] == "POST"){
            $login = $_POST['login'];
            $password = $_POST['password'];

            if(User::login($login, $password)){
                $_SESSION['auth']['login'] = $login;
                $_SESSION['auth']['password'] = $password;
                //print_r($_SESSION['auth']);
                header('Location: http://'.$_SERVER['HTTP_HOST'].'/');
            }else{
                return exit('Not correct');
            }
        }
        //Подключение вида
        require_once(ROOT . '/views/admin/login.php');
        return true;
    }

    public function actionLogout()
    {
        unset($_SESSION['auth']);
        header('Location: http://'.$_SERVER['HTTP_HOST'].'/');
        session_destroy();

    }




}