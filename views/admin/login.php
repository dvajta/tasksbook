<!doctype html>
<html lang="ru">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Вход в админку</h1>
            <form action="/admin/login" method="post">
                <div class="form-group">
                    <label for="login">Имя пользователя*</label>
                    <input type="text" class="form-control" placeholder="Введите имя пользователя" name="login">
                </div>
                <div class="form-group">
                    <label for="password">Пароль*</label>
                    <input type="password" class="form-control" placeholder="Введите пароль" name="password">
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>