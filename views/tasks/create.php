<!doctype html>
<html lang="ru">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="/style.css" rel="stylesheet">
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row" id="taskForm">
        <div class="col-md-12">
            <h1>Добавить задачу</h1>
            <form action="/tasks/create" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="file" name="image">
                </div>
                <div class="form-group">
                    <label for="userName">Имя пользователя*</label>
                    <input type="text" class="form-control" id="userName" placeholder="Введите ваше имя" name="name">
                </div>
                <div class="form-group">
                    <label for="userEmail">E-mail*</label>
                    <input type="text" class="form-control" id="userEmail" placeholder="Введите ваш e-mail" name="email">
                </div>
                <div class="form-group">
                    <label for="taskText">Текст задачи*</label>
                    <textarea name="text" id="taskText" class="form-control" cols="30" rows="10" placeholder="Подробно опишите задачу..."></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Сохранить</button>
                </div>
            </form>
            <a href="#" class="btn btn-default preview-task">Предпросмотр</a>
        </div>
    </div>
    <div class="row hidden" id="itemTask">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Текст</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="nameUserTask"></td>
                        <td id="emailUserTask"></td>
                        <td id="textUserTask"></td>
                        <td><a href="#" class="btn btn-default create-task">Вернуться</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
<script>
    $('.preview-task').on('click', function(e){
        e.preventDefault();
        var name = document.querySelector('#userName').value,
            email = document.querySelector('#userEmail').value,
            text = document.querySelector('#taskText').value;

        $('#itemTask').removeClass('hidden');
        $('#taskForm').addClass('hidden');
        document.querySelector('#nameUserTask').innerHTML = name;
        document.querySelector('#emailUserTask').innerHTML = email;
        document.querySelector('#textUserTask').innerHTML = text;

    })

    $('.create-task').on('click', function(e){
        e.preventDefault();

        $('#itemTask').addClass('hidden');
        $('#taskForm').removeClass('hidden');

    })
</script>