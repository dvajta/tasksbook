<?php
/**
 * @var $tasksList array
 * @var $pagination object
 */

?>
<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <?php if(isset($_SESSION['auth'])): ?>
    <div class="row">
        <div class="col-md-6" style="background-color:#000; color:#fff; padding:10px">Панель администратора</div>
        <div class="col-md-6" style="background-color:#000; padding:10px; text-align:right"><a style="color:#fff;" href="/admin/logout">Выйти (<?=$_SESSION['auth']['login'] ?>)</a></div>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-12">
            <div style="margin-bottom:20px">
            <h1>Все задачи</h1>
            <a href="/tasks/create" class="btn btn-success">Добавить задачу</a>
            </div>
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                    Сортировка
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" >
                    <li><a href="/">По умолчанию</a></li>
                    <li><a href="/tasks/?sort=name&order=ASC">Имя(А-Я)</a></li>
                    <li><a href="/tasks/?sort=name&order=DESC">Имя(Я-А)</a></li>
                    <li><a href="/tasks/?sort=email&order=ASC">Email(А-Я)</a></li>
                    <li><a href="/tasks/?sort=email&order=DESC">Email(Я-А)</a></li>
                    <li><a href="/tasks/?status=1/">Статус(Выполненные)</a></li>
                    <li><a href="/tasks/?status=0/">Статус(Невыполненные)</a></li>
                </ul>
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Картинка</th>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Текст</th>
                    <th>Статус</th>
                    <?php if(isset($_SESSION['auth'])): ?>
                    <th>Действия</th>
                    <?php endif; ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach($tasksList as $task): ?>
                    <tr>
                        <td><?=$task['id']?></td>
                        <td><img src="/uploads/<?=$task['image']?>" alt="<?=$task['image']?>"></td>
                        <td><?=$task['name']?></td>
                        <td><?=$task['email']?></td>
                        <td><?=$task['text']?></td>
                        <td><?=$task['status'] == 1 ? 'Выполнено' : 'Не выполнено'?></td>
                        <?php if(isset($_SESSION['auth'])): ?>
                        <td>
                            <a href="/tasks/update/<?=$task['id']?>" class="btn btn-warning">Редактировать</a>
                            <a onclick="return confirm('are you sure?')" href="/tasks/delete/<?=$task['id']?>" class="btn btn-danger">Удалить</a>
                        </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <!-- Постраничная навигация -->
            <div class="pagination">
                <?=$pagination->get(); ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>