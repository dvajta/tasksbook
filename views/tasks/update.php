<?php


?>

<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <?php if(isset($_SESSION['auth'])): ?>
        <div class="row">
            <div class="col-md-6" style="background-color:#000; color:#fff; padding:10px">Панель администратора</div>
            <div class="col-md-6" style="background-color:#000; padding:10px; text-align:right"><a style="color:#fff;" href="/admin/logout">Выйти (<?=$_SESSION['auth']['login'] ?>)</a></div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-12">

            <a href="/">Вернуться назад</a>
            <form action="/tasks/update/<?=$task['id']?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <img style="margin-bottom:10px" src="/uploads/<?=$task['image']?>" alt="<?=$task['image']?>">
                    <input type="file" name="image">
                </div>
                <div class="form-group">
                    <label for="userName">Имя пользователя*</label>
                    <input type="text" class="form-control" id="userName" placeholder="Введите ваше имя" name="name" value="<?=$task['name']?>">
                </div>
                <div class="form-group">
                    <label for="userEmail">E-mail*</label>
                    <input type="text" class="form-control" id="userEmail" placeholder="Введите ваш e-mail" name="email" value="<?=$task['email']?>">
                </div>
                <div class="form-group">
                    <label for="taskText">Текст задачи*</label>
                    <textarea name="text" id="taskText" class="form-control" cols="30" rows="10" placeholder="Подробно опишите задачу..."><?=$task['text']?></textarea>
                </div>
                <div class="form-group">
                    <label for="userEmail">Статус</label>
                    <input type="checkbox" id="status" name="status" <?=$task['status'] == 1 ? 'checked' : false ?>>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>