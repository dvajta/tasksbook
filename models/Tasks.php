<?php
namespace models;

use components\Db;
use PDO;

class Tasks
{
    const SHOW_BY_DEFAULT = 3;
    /**
     * Returns single task with specified id
     */
    public static function getTasksItemById($id)
    {
        //Подключаемся к бд
        $db = Db::getConnection();
        //Составляем sql-запрос
        $sql = "SELECT * FROM tasks WHERE id = :id";
        //Подготавливаем запрос к выполнению
        $statement = $db->prepare($sql);
        $statement->bindParam(':id',$id);
        //Выполняем запрос
        $statement->execute();
        //Получаем результат в виде ассоциативного массива
        $results = $statement->fetch(PDO::FETCH_ASSOC);

        return $results;
    }

    public static function updateTask($data)
    {
        $db = Db::getConnection();
        //Формируем sql-запрос
        $sql = "UPDATE tasks SET name = :name, email = :email, text = :text, image = :image, status = :status WHERE id = :id";

        $statement = $db->prepare($sql);

        return $statement->execute($data);
    }

    /**
     * @param $id
     * @return bool
     */
    public static function deleteTask($id)
    {
        $db = Db::getConnection();
        //Формируем sql-запрос
        $sql = "DELETE FROM tasks WHERE id = :id";

        $statement = $db->prepare($sql);
        $statement->bindParam(':id',$id);

        return $statement->execute();
    }

    /**
     * Returns total tasks
     */
    public static function getTotalTasks()
    {
        //Подключаемся к бд
        $db = Db::getConnection();

        $result = $db->query("SELECT count(id) AS count FROM tasks ");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();

        return $row['count'];
    }

    /**
     * Returns an array of tasks items with sorting
     * @param $sort
     * @param $order
     * @param $status
     * @param $page
     * @return array
     */
    public static function getTasksBySort($sort, $order, $page)
    {
        //Подключаемся к бд
        $db = Db::getConnection();
        $offset = self::getOffset($page);

        $result = $db->query("SELECT * FROM tasks ORDER BY $sort $order LIMIT ".self::SHOW_BY_DEFAULT." OFFSET ". $offset);
        return self::compileTasksList($result);

    }

    /**
     * Returns an array of tasks by status
     * @param $status
     * @param $page
     * @return array
     */
    public static function getTasksByStatus($status, $page)
    {
        //Подключаемся к бд
        $db = Db::getConnection();
        $offset = self::getOffset($page);
        $result = $db->query("SELECT * FROM tasks WHERE status = $status LIMIT ".self::SHOW_BY_DEFAULT." OFFSET ". $offset);
        return self::compileTasksList($result);

    }

    /**
     * Returns an array of tasks items
     * @param $page
     */
    public static function getTasksList($page)
    {
        //Подключаемся к бд
        $db = Db::getConnection();
        $offset = self::getOffset($page);

        $result = $db->query("SELECT * FROM tasks LIMIT ".self::SHOW_BY_DEFAULT." OFFSET ". $offset);
        return self::compileTasksList($result);

    }

    /**
     * @param $page
     * @return float|int
     */
    static function getOffset($page)
    {
        $page = intval($page);
        return ($page - 1) * self::SHOW_BY_DEFAULT;

    }

    /**
     * Return an array  compile Tasks List
     * @param $result
     * @return array
     */
    static function compileTasksList($result)
    {
        $tasksList = [];
        $i = 0;
        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            $tasksList[$i]['id'] = $row['id'];
            $tasksList[$i]['name'] = $row['name'];
            $tasksList[$i]['email'] = $row['email'];
            $tasksList[$i]['text'] = $row['text'];
            $tasksList[$i]['image'] = $row['image'];
            $tasksList[$i]['status'] = $row['status'];
            $i++;

        }
        return $tasksList;
    }

    /**
     * Create new task
     */
    public static function createTask($data){
        //Подключаемся к бд
        $db = Db::getConnection();
        //Формирование sql - запроса
        $sql = "INSERT INTO tasks (name, email, text, image) value (:name, :email, :text, :image)";
        //Подготавливаем запрос
        $statement = $db->prepare($sql);
        return $statement->execute($data);

    }
}