<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 08.02.2020
 * Time: 16:46
 */

namespace models;


use components\Db;
use PDO;

class User
{

    public static function login($username, $password)
    {
        //Подключаемся к бд
        $db = Db::getConnection();
        //Составляем sql-запрос
        $sql = "SELECT * FROM users WHERE username = :username AND password = :password";
        //Подготавливаем запрос к выполнению
        $statement = $db->prepare($sql);
        $statement->bindParam(':username',$username);
        $statement->bindParam(':password',$password);
        //Выполняем запрос
        $statement->execute();
        //Получаем результат в виде ассоциативного массива
        $results = $statement->fetch(PDO::FETCH_ASSOC);
        if($results){
            return true;
        }else{
            return false;
        }

    }
}