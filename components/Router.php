<?php

namespace components;
session_start();
class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     * Returns request string
     */
    private function getURI()
    {
        if(!empty($_SERVER['REQUEST_URI'])){
            return trim($_SERVER['REQUEST_URI'], '/');
        }
        return true;
    }

    public function run()
    {
        //Получаем строку запроса
        $uri = $this->getURI();

        //Проверяем наличие такого запроса в routes.php
        foreach($this->routes as $uriPattern => $path){

            // Сравниваем $uriPattern и $uri
            if(preg_match("~$uriPattern~", $uri)){

                //Получаем внутренний путь из внешнего согласно правилу.
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
//print_r($internalRoute);
                //Определяем контроллер, action, параметры


                //Если есть совпадение, определить какой контроллер и action обрабатывают запрос
                $segments = explode('/', $internalRoute);
                $controllerName = ucfirst(array_shift($segments).'Controller');
                $actionName = 'action'. ucfirst(array_shift($segments));
 //print_r($actionName);
                $parameters = $segments;

                //Подключаем файл класса-контроллера
                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
                if(file_exists($controllerFile)){
                    include_once($controllerFile);
                }

                //Создаём объект, вызваем метод
                $controller = 'controllers\\'.$controllerName;
                $controllerObject = new $controller;
                $result = call_user_func_array([$controllerObject, $actionName], $parameters);
                //print_r($result);
                if($result != null){
                    break;
                }
            }
        }

    }
}